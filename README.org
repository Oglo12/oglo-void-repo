* Oglo Void Repository
OgloTheNerd's Void Linux software repository.

** How To Use
1. Run these commands as root:

#+begin_src
echo "repository=https://gitlab.com/Oglo12/oglo-void-repo/-/raw/main/binpkgs/x86_64" > /etc/xbps.d/oglo-void-repo.conf
xbps-install -S
#+end_src
